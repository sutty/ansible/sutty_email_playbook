Sutty Email Playbook
--------------------

Base Ansible playbook for Sutty's email system.

* Sending email for services (container monits, CMS, etc.)
* Forwarding addresses for any domain hosted.

Inventory
---------

Create, link or clone an `inventory.yml` file with the host roles.

The `sutty_rspamd` group should contain a single host which will run the
rspamd service.  The `sutty_email` group runs postfix services that
check spam to this service.

If you want to rotate roles, change the `sutty_rspamd` host and the
playbook will stop previous containers.  It doesn't copy data yet.

```yaml
---
sutty_email:
  hosts:
    host.name:
    host2.name:
sutty_rspamd:
  hosts:
    host.name:
```

Host vars
---------

Create, link or clone a directory `host_vars/` with a YAML file for each
host, containing all host-specific variables.

On `host_vars/host.name.yml`:

```yaml
---
custom_variable: "value"
```
